import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

type IconProps = React.PropsWithChildren<{
  name: string;
  color: string;
  size: number;
}>;

const RNIcon = ({name, color, size, ...rest}: IconProps) => {
  return <Icon name={name} size={size} color={color} {...rest} />;
};

export default RNIcon;
