import TrackPlayer, {Capability} from 'react-native-track-player';
import {MMKV} from 'react-native-mmkv';

const isPlayerInitialized = async () => {
  const storage = new MMKV();
  const isPlayerSetup = storage.getBoolean('isPlayerSetup');

  if (!isPlayerSetup) {
    try {
      await TrackPlayer.setupPlayer();

      await TrackPlayer.updateOptions({
        capabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.SkipToNext,
          Capability.SkipToPrevious,
          Capability.SeekTo,
        ],
        compactCapabilities: [
          Capability.Play,
          Capability.Pause,
          Capability.SkipToNext,
        ],
        progressUpdateEventInterval: 2,
      });

      storage.set('isPlayerSetup', true);
    } catch (error) {
      console.log('Error: To initialize TrackPlayer', error);
    }
  }
};

export default isPlayerInitialized;

export const storageWrapper = {
  storage: new MMKV(),

  setItem: (key, value) => {
    storageWrapper.storage.set(
      key,
      typeof value === 'string' ? value : JSON.stringify(value),
    );
  },

  getItem: (key, defaultValue) => {
    const value = storageWrapper.storage.getString(key);
    return value || defaultValue;
  },
};
