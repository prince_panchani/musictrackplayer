import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// screens
import Home from '../screens/home/home';
import UserLibrary from '../screens/library/userLibrary';

import AntIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IonIcon from 'react-native-vector-icons/Ionicons';

// styles
import {GlobalStyles} from '../constants/style';

const BottomTabNavigation = () => {
  const Tab = createBottomTabNavigator();
  const {
    colors: {black, white},
  } = GlobalStyles;

  const renderEmptyHomeIcon = () => (
    <AntIcon name="home" size={30} color={black} />
  );

  const renderFilledHomeIcon = () => (
    <EntypoIcon name="home" size={30} color={white} />
  );

  const renderFilledUserIcon = () => (
    <IonIcon name="library" size={30} color={white} />
  );
  const renderEmptyUserIcon = () => (
    <IonIcon name="library-outline" size={30} color={black} />
  );

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
          shadowOpacity: 4,
          shadowRadius: 4,
          elevation: 4,
          shadowOffset: {
            width: 0,
            height: -4,
          },
          borderRadius: 0,
          backgroundColor: '#c4c4c4',
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          headerShown: false,
          tabBarLabelStyle: {
            color: white,
          },
          tabBarIcon: ({focused}) => {
            if (focused) {
              return renderFilledHomeIcon();
            } else {
              return renderEmptyHomeIcon();
            }
          },
        }}
      />
      <Tab.Screen
        name="Your Library"
        component={UserLibrary}
        options={{
          tabBarLabel: 'Your Library',
          headerShown: false,
          tabBarLabelStyle: {
            color: white,
          },
          tabBarIcon: ({focused}) => {
            if (focused) {
              return renderFilledUserIcon();
            } else {
              return renderEmptyUserIcon();
            }
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTabNavigation;
