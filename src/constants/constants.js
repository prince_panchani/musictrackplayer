export const defaultSongs = [
  {
    songId: 1,
    url: './music/best-flute-2924.mp3',
    name: 'Flute',
  },
  {
    songId: 2,
    url: './music/roja-flute-18398.mp3',
    name: 'Flute-sweet',
  },
];

export const modalColors = [
  '#27374D',
  '#1D267D',
  '#BE5A83',
  '#212A3E',
  '#917FB3',
  '#37306B',
  '#443C68',
  '#5B8FB9',
  '#144272',
];

export const tabItems = [
  {
    id: 'tab1',
    title: 'Favorites',
  },
  {
    id: 'tab2',
    title: 'Playlist',
  },
];
