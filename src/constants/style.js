export const GlobalStyles = {
  colors: {
    black: '#000',
    white: '#fff',
    primary50: '#1DB954',
    secondary50: '#42275a',
    gray50: '#5072A7',
  },
};
