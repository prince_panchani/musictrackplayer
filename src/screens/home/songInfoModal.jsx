import React from 'react';
import PropTypes from 'prop-types';
import TrackPlayer from 'react-native-track-player';
import {Image, Modal, Text, TouchableOpacity, View} from 'react-native';

import RNIcon from '../../components/RNIcon';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import styles from './styles/home';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';

import {GlobalStyles} from '../../constants/style';

const SongInfoModal = ({
  closeModal,
  currentTrack,
  isSongPlaying,
  pausePlayingSong,
  isModalVisible,
  playTrack,
  songs,
  setCurrentTrack,
  setIsSongPlaying,
}) => {
  const {
    colors: {primary50},
  } = GlobalStyles;

  const {url: currentSongUrl} = currentTrack ?? {};

  const playSong = async song => {
    setIsSongPlaying(true);
    await TrackPlayer.reset();
    await TrackPlayer.add({
      url: song.url,
    });
    await TrackPlayer.play();
    setCurrentTrack(song);
  };

  const playPreviousSong = async () => {
    const currentIndex = songs.findIndex(({url}) => url === currentSongUrl);
    let previousIndex = currentIndex - 1;

    if (previousIndex < 0) {
      previousIndex = songs.length - 1;
    }

    const previousSong = songs[previousIndex];
    playSong(previousSong);
  };

  const playNextSong = async () => {
    const currentIndex = songs.findIndex(({url}) => url === currentSongUrl);
    let nextIndex = currentIndex + 1;

    if (nextIndex === songs.length) {
      nextIndex = 0;
    }

    const nextSong = songs[nextIndex];
    playSong(nextSong);
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isModalVisible}
      onRequestClose={closeModal}>
      <View style={[styles.modalView]}>
        <View style={styles.modalContentContainer}>
          <TouchableOpacity onPress={closeModal}>
            <EntypoIcon
              name="chevron-thin-down"
              color={styles.primaryText}
              size={24}
            />
          </TouchableOpacity>
          <Text style={styles.primaryText}>Barsat ki dhun</Text>
          <TouchableOpacity>
            <EntypoIcon
              name="dots-three-vertical"
              size={24}
              color={styles.primaryText}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.modalImgContainer}>
          <Image
            source={require('../../assets/images/image.png')}
            style={styles.modalImg}
          />
          <View style={styles.generalContainer}>
            <View style={styles.innerModalContainer}>
              <Text style={styles.songTitle} numberOfLines={1}>
                {currentTrack !== null ? currentTrack?.name : null}
              </Text>
              <Text numberOfLines={1} style={styles.primaryText}>
                Rochak Kohli, Jubin Nautiyal andAnu Malik.
              </Text>
            </View>
            <AntIcon name="heart" size={24} color={primary50} />
          </View>
        </View>

        <View style={styles.generalContainer}>
          <TouchableOpacity>
            <RNIcon name="arrows" size={30} color={primary50} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={playPreviousSong}
            disabled={currentTrack === songs[0]}>
            <IonIcon
              name="play-skip-back-sharp"
              color={styles.primaryText}
              size={30}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            {isSongPlaying ? (
              <TouchableOpacity onPress={pausePlayingSong}>
                <AntIcon name="pausecircle" size={60} color="white" />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={styles.pauseSongContainer}
                onPress={playTrack}>
                <EntypoIcon name="controller-play" size={26} color="black" />
              </TouchableOpacity>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={playNextSong}
            disabled={currentTrack === songs[songs.length - 1]}>
            <IonIcon
              name="play-skip-forward"
              color={styles.primaryText}
              size={30}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <FeatherIcon name="repeat" color={primary50} size={30} />
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

SongInfoModal.propTypes = {
  isSongPlaying: PropTypes.bool,
  setIsSongPlaying: PropTypes.func,
  currentTrack: PropTypes.shape({}),
  setCurrentTrack: PropTypes.func,
  closeModal: PropTypes.func,
  pausePlayingSong: PropTypes.func,
  playTrack: PropTypes.func,
  isModalVisible: PropTypes.bool,
  songs: PropTypes.arrayOf(PropTypes.shape({})),
};

SongInfoModal.defaultProps = {
  currentTrack: {},
  setCurrentTrack: () => {},
  isSongPlaying: false,
  setIsSongPlaying: () => {},
  closeModal: () => {},
  playTrack: () => {},
  pausePlayingSong: () => {},
  isModalVisible: false,
  songs: [],
};

export default SongInfoModal;
