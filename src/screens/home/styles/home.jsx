import {StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';
import {Dimensions} from 'react-native';

const {
  colors: {gray50, secondary50, primary50, white, black},
} = GlobalStyles;

const windowWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  InnerContainer: {
    flex: 1,
    marginTop: 10,
    paddingHorizontal: 6,
  },
  searchSongContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 8,
    borderRadius: 4,
    backgroundColor: secondary50,
  },
  primaryText: {
    color: white,
  },
  songsWrapper: {
    marginTop: 8,
  },
  heading: {
    fontSize: 20,
    color: white,
    fontWeight: '800',
  },
  songItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
    marginTop: 8,
    paddingHorizontal: 8,
    position: 'relative',
  },
  playlistContainer: {
    position: 'absolute',
    right: 12,
    borderRadius: 4,
    backgroundColor: white,
    top: 20,
    paddingVertical: 6,
    paddingHorizontal: 10,
  },
  playlistLabel: {
    color: black,
  },
  stickyBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
  },
  image: {
    height: 40,
    width: 40,
  },
  songDescription: {
    color: white,
    width: '70%',
  },
  controllerPlayerContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  controllerPlayer: {
    width: 60,
    height: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: primary50,
  },
  iconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: 6,
  },
  playTrackContainer: {
    backgroundColor: gray50,
    width: '90%',
    marginRight: 'auto',
    marginLeft: 'auto',
    left: 20,
    bottom: 60,
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'center',
    gap: 10,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 10,
    zIndex: -1,
  },
  playTrackSongDesc: {
    color: white,
    fontWeight: 'bold',
  },
  stickyBoxSongDescription: {
    color: white,
    fontWeight: 'bold',
    width: '60%',
  },
  modalView: {
    alignItems: 'center',
    shadowColor: black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    height: '100%',
    width: '100%',
    backgroundColor: gray50,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  modalContentContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
  },
  modalImgContainer: {
    marginTop: 10,
    padding: 10,
  },
  modalImg: {
    resizeMode: 'cover',
    width: windowWidth - 10,
    height: 320,
    borderRadius: 4,
  },
  generalContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    width: windowWidth - 10,
  },
  innerModalContainer: {
    flex: 1,
  },
  songTitle: {
    color: white,
    fontWeight: '800',
    fontSize: 18,
    width: windowWidth - 40,
  },
  pauseSongContainer: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
