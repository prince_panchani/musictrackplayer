import {Image, Text, TouchableOpacity, View} from 'react-native';
import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import TrackPlayer, {State} from 'react-native-track-player';
import RNFS from 'react-native-fs';
import uuid from 'react-native-uuid';
import {MMKV} from 'react-native-mmkv';
import styled from 'styled-components/native';

// components
import SongInfoModal from './songInfoModal';
import SongList from './songList';

// styles and helpers
import AntIcon from 'react-native-vector-icons/AntDesign';
import styles from './styles/home';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import {GlobalStyles} from '../../constants/style';
import {defaultSongs} from '../../constants/constants';
import {
  BottomSheetModal,
  BottomSheetView,
  BottomSheetModalProvider,
} from '@gorhom/bottom-sheet';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const Home = ({navigation}) => {
  const {
    colors: {primary50},
  } = GlobalStyles;
  const [isModalVisible, setModalVisible] = useState(false);
  const [isSongPlaying, setIsSongPlaying] = useState(false);
  const [songs, setSongs] = useState([]);
  const [currentTrack, setCurrentTrack] = useState(null);
  const [likedSongs, setLikedSongs] = useState([]);
  const bottomSheetModalRef = useRef(null);

  const snapPoints = useMemo(() => ['25%', '50%', '100%'], []);

  const fetchSongsFromLocalDevice = useCallback(async () => {
    try {
      let musicFolderPath = `${RNFS.ExternalStorageDirectoryPath}/Music`;
      const result = await RNFS.readDir(musicFolderPath);

      const modifiedSongs = result.map(song => {
        const songId = uuid.v4();
        const {path, ...rest} = song ?? {};
        return {
          ...rest,
          songId,
          url: song?.path,
        };
      });

      if (modifiedSongs.length) {
        setSongs(modifiedSongs);
        setCurrentTrack(modifiedSongs[0]);
      }
    } catch (error) {
      console.log('Error: To fetch the songs.', error);
    }
  }, []);

  useEffect(() => {
    const storage = new MMKV();

    const hasValidPermission = storage.getBoolean(
      'hasExternalStoragePermission',
    );
    if (hasValidPermission) {
      fetchSongsFromLocalDevice();
    }
    setSongs(defaultSongs);
    setCurrentTrack(defaultSongs[0]);
  }, [fetchSongsFromLocalDevice]);

  const playTrack = async () => {
    try {
      const state = await TrackPlayer.getState();

      if (state === State.Playing) {
        setIsSongPlaying(false);
        await TrackPlayer.pause();
      } else {
        await TrackPlayer.add({
          url: currentTrack.url,
        });

        await TrackPlayer.play();
        setIsSongPlaying(true);
      }
    } catch (error) {
      setIsSongPlaying(false);
      console.log('Error: To play the song', error);
    }
  };

  const pausePlayingSong = async () => {
    try {
      const state = await TrackPlayer.getState();

      if (state === State.Playing) {
        setIsSongPlaying(false);
        await TrackPlayer.pause();
      }
    } catch (error) {
      setIsSongPlaying(false);
      console.log('Error: To pause the song', error);
    }
  };

  const playCurrentSong = async songInfo => {
    await TrackPlayer.reset();
    await TrackPlayer.add({
      url: songInfo.url,
    });
    await TrackPlayer.play();
    setIsSongPlaying(true);
    setCurrentTrack(songInfo);
  };

  const openModal = useCallback(() => {
    setModalVisible(true);
  }, []);

  const closeModal = () => {
    setModalVisible(false);
  };

  useEffect(() => {
    const storage = new MMKV();
    const storedLikedSongs = storage.getString('likedSongs');
    if (storedLikedSongs) {
      setLikedSongs(JSON.parse(storedLikedSongs));
    }
  }, []);

  const isSongLiked = useCallback(
    song => {
      return likedSongs.some(likedSong => likedSong.url === song.url);
    },
    [likedSongs],
  );

  const toggleLikedSong = useCallback(song => {
    const storage = new MMKV();

    const songList = storage.getString('likedSongs');
    const likedSongsArray = songList ? JSON.parse(songList) : [];

    let updatedLikedSongs = [...likedSongsArray];
    const index = likedSongsArray.findIndex(
      likedSong => likedSong.url === song.url,
    );
    if (index !== -1) {
      updatedLikedSongs.splice(index, 1);
    } else {
      updatedLikedSongs.push(song);
    }
    setLikedSongs(updatedLikedSongs);
    storage.set('likedSongs', JSON.stringify(updatedLikedSongs));
  }, []);

  const isSongAlreadyLiked = useMemo(
    () => currentTrack && isSongLiked(currentTrack),
    [currentTrack, isSongLiked],
  );

  const handleSongToAddInPlayList = () => {
    const storage = new MMKV();
    const playList = storage.getString('userPlayList');
    if (playList !== undefined) {
      console.log('userPlayList', JSON.parse(playList));
      navigation.navigate('PlayListScreen');
    }
  };

  // const Container = styled.View`
  //   flex: 1;
  //   position: 'relative';
  //   // background-color: linear-gradient(#614385, #516395);
  //   background-color: #614385;
  // `;

  const Container = styled(LinearGradient).attrs({
    colors: ['#614385', '#516395'],
    start: {x: 0, y: 0},
    end: {x: 1, y: 1},
    flex: 1,
    position: 'relative',
  })``;

  return (
    <>
      <GestureHandlerRootView style={styles.container}>
        <SafeAreaProvider>
          <Container>
            <View style={styles.InnerContainer}>
              <View style={styles.songsWrapper}>
                <Text style={styles.heading}>Trending right now</Text>

                <TouchableOpacity style={styles.controllerPlayerContainer}>
                  {isSongPlaying ? (
                    <TouchableOpacity
                      onPress={pausePlayingSong}
                      style={styles.controllerPlayer}>
                      <AntIcon
                        name="pausecircle"
                        size={24}
                        color={styles.primaryText}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={styles.controllerPlayer}
                      onPress={playTrack}>
                      <EntypoIcon
                        name="controller-play"
                        color={styles.primaryText}
                        size={24}
                      />
                    </TouchableOpacity>
                  )}
                </TouchableOpacity>
              </View>

              <SongList
                playCurrentSong={playCurrentSong}
                songs={songs}
                toggleLikedSong={toggleLikedSong}
                isSongLiked={isSongLiked}
              />
            </View>
          </Container>

          {currentTrack !== null ? (
            <TouchableOpacity
              style={styles.playTrackContainer}
              onPress={openModal}>
              <View style={styles.stickyBoxContainer}>
                <Image
                  source={require('../../assets/images/image.png')}
                  style={styles.image}
                />
                <Text style={styles.stickyBoxSongDescription} numberOfLines={2}>
                  {currentTrack !== null ? currentTrack?.name : null}
                </Text>
              </View>

              <View style={styles.iconContainer}>
                <TouchableOpacity onPress={() => toggleLikedSong(currentTrack)}>
                  {isSongAlreadyLiked ? (
                    <AntIcon name="heart" size={24} color={primary50} />
                  ) : (
                    <AntIcon name="hearto" size={24} color={primary50} />
                  )}
                </TouchableOpacity>
                <TouchableOpacity>
                  {isSongPlaying ? (
                    <TouchableOpacity onPress={pausePlayingSong}>
                      <AntIcon
                        name="pausecircle"
                        size={24}
                        color={styles.primaryText}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity onPress={playTrack}>
                      <EntypoIcon
                        name="controller-play"
                        color={styles.primaryText}
                        size={24}
                      />
                    </TouchableOpacity>
                  )}
                </TouchableOpacity>
              </View>
            </TouchableOpacity>
          ) : null}

          <BottomSheetModalProvider>
            <BottomSheetModal
              ref={bottomSheetModalRef}
              index={1}
              snapPoints={snapPoints}>
              <BottomSheetView>
                <Text>Awesome 🎉</Text>
              </BottomSheetView>
            </BottomSheetModal>
          </BottomSheetModalProvider>

          {isModalVisible ? (
            <SongInfoModal
              closeModal={closeModal}
              currentTrack={currentTrack}
              isSongPlaying={isSongPlaying}
              pausePlayingSong={pausePlayingSong}
              isModalVisible={isModalVisible}
              playTrack={playTrack}
              songs={songs}
              setCurrentTrack={setCurrentTrack}
              setIsSongPlaying={setIsSongPlaying}
            />
          ) : null}
        </SafeAreaProvider>
      </GestureHandlerRootView>
    </>
  );
};

export default Home;
