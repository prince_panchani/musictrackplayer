import React from 'react';
import {Image, Text, View, FlatList, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

import styles from './styles/home';
import {GlobalStyles} from '../../constants/style';

import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';

const SongList = ({playCurrentSong, songs, isSongLiked, toggleLikedSong}) => {
  const {
    colors: {primary50},
  } = GlobalStyles;

  const renderSongItem = ({item}) => {
    const isSongAlreadyLiked = isSongLiked(item);

    return (
      <>
        <TouchableOpacity
          style={styles.songItemContainer}
          onPress={() => playCurrentSong(item)}>
          <Image
            source={require('../../assets/images/image.png')}
            style={styles.image}
            resizeMode="cover"
          />
          <Text numberOfLines={3} style={styles.songDescription}>
            {item.name}
          </Text>
          <View style={styles.iconContainer}>
            <TouchableOpacity onPress={() => toggleLikedSong(item)}>
              {isSongAlreadyLiked ? (
                <AntIcon name="heart" size={24} color={primary50} />
              ) : (
                <AntIcon name="hearto" size={24} color={primary50} />
              )}
            </TouchableOpacity>
            <TouchableOpacity>
              <EntypoIcon
                name="dots-three-vertical"
                size={24}
                color={styles.primaryText}
              />
            </TouchableOpacity>
          </View>
        </TouchableOpacity>
      </>
    );
  };

  return (
    <FlatList
      data={songs}
      keyExtractor={item => item.songId}
      renderItem={renderSongItem}
    />
  );
};

SongList.propTypes = {
  playCurrentSong: PropTypes.func.isRequired,
  toggleLikedSong: PropTypes.func.isRequired,
  songs: PropTypes.array,
  isSongLiked: PropTypes.func,
};

SongList.default = {
  isSongLiked: () => {},
  songs: [],
};

export default SongList;
