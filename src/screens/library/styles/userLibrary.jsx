import {Dimensions, StyleSheet} from 'react-native';
import {GlobalStyles} from '../../../constants/style';

const windowWidth = Dimensions.get('window').width;

const {
  colors: {white, black},
} = GlobalStyles;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2d2e37',
  },
  imageContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    position: 'relative',
    marginTop: 6,
  },
  image: {
    width: windowWidth - 10,
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10,
  },
  textContainer: {
    position: 'absolute',
    left: 40,
    top: 40,
  },
  cardHeading: {
    color: white,
    fontWeight: '600',
    fontSize: 14,
  },
  generalText: {
    color: white,
    fontWeight: '800',
    fontSize: 38,
  },
  tabContainer: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
    marginHorizontal: 4,
    marginVertical: 6,
    borderRadius: 8,
  },
  activeTabBg: {
    backgroundColor: '#3c3abe',
  },
  item: {
    color: white,
    fontSize: 14,
    fontWeight: '600',
  },
  albumContainer: {
    flex: 1,
    paddingVertical: 8,
  },
  albumItem: {
    margin: 10,
    alignItems: 'flex-start',
    flex: 1,
  },
  albumImg: {
    width: '100%',
    height: 110,
    borderRadius: 6,
    position: 'relative',
  },
  albumHeadingText: {
    color: white,
    fontSize: 16,
    fontWeight: '600',
  },
  albumContentText: {
    color: '#c4c4c4',
    fontSize: 14,
    fontWeight: '400',
  },
  albumContentContainer: {
    width: windowWidth - 188,
  },
  controlIconWrapper: {
    position: 'absolute',
    right: 6,
    bottom: 50,
  },

  playListContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    alignItems: 'flex-start',
  },
  playListGeneralText: {
    color: white,
    fontSize: 18,
    fontWeight: '800',
  },
  playListSecondaryText: {
    color: '#c4c4c4',
    fontSize: 14,
    fontWeight: '600',
  },
  addNewPlayListContainer: {
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#3c3abe',
    paddingHorizontal: 75,
    paddingVertical: 6,
    borderRadius: 8,
    marginTop: 8,
  },
  addNewPlayListText: {
    color: white,
    fontSize: 14,
  },
  centeredView: {
    alignItems: 'center',
    shadowColor: black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    height: '100%',
    width: '100%',
    backgroundColor: 'gray',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  modalView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  playlistInput: {
    color: white,
    borderBottomWidth: 1,
    borderBottomColor: white,
    width: windowWidth - 80,
    fontSize: 20,
    textAlign: 'center',
    fontWeight: '800',
  },
  closeIcon: {
    position: 'absolute',
    top: 4,
    right: 4,
  },
  btnWrapper: {
    backgroundColor: '#3c3abe',
    width: windowWidth - 80,
    alignItems: 'center',
    marginTop: 10,
    height: 30,
    justifyContent: 'center',
    borderRadius: 6,
  },
  addPlayListBtn: {
    color: white,
  },
  title: {
    color: white,
    fontSize: 22,
    textAlign: 'left',
    marginBottom: 10,
    fontWeight: '800',
  },

  userPlayListContainer: {
    flex: 1,
    paddingHorizontal: 8,
    marginTop: 8,
  },
  userListItemWrapper: {
    flex: 1,
    gap: 8,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  playListImg: {
    height: 60,
    width: 60,
    borderRadius: 6,
  },
  innerWrapper: {
    height: '100%',
    marginVertical: 6,
  },
  playListInfoWrapper: {
    flex: 1,
    flexDirection: 'row',
    gap: 8,
  },
  dot: {
    backgroundColor: white,
    height: 5,
    width: 5,
    borderRadius: 6,
    marginTop: 8,
  },
  playListName: {
    color: white,
    fontSize: 18,
    fontWeight: '600',
  },
  text: {
    color: white,
  },
});

export default styles;
