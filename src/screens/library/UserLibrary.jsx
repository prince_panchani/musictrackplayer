import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, Image, Text, TouchableOpacity, View} from 'react-native';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';

import styles from './styles/userLibrary';
import {MMKV} from 'react-native-mmkv';
import {tabItems} from '../../constants/constants';
import TrackPlayer, {State} from 'react-native-track-player';
import AddPlayList from './addPlayList';

const UserLibrary = () => {
  const [userFavoriteSongs, setUserFavoriteSongs] = useState([]);
  const [selectedTab, setSelectedTab] = useState(tabItems[0].id);
  const tabBarHeight = useBottomTabBarHeight();
  const [currentTrack, setCurrentTrack] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [playList, setPlayList] = useState([]);

  useEffect(() => {
    const storage = new MMKV();
    const favoriteSongs = storage.getString('likedSongs');
    if (favoriteSongs) {
      setUserFavoriteSongs(JSON.parse(favoriteSongs));
    } else {
      setUserFavoriteSongs([]);
    }
  }, []);

  const handleTabChange = tabId => {
    setSelectedTab(tabId);
  };

  const playSong = useCallback(async song => {
    try {
      const state = await TrackPlayer.getState();

      if (state === State.Playing) {
        await TrackPlayer.reset();
        setCurrentTrack(null);
      } else {
        await TrackPlayer.add({
          url: song.url,
        });
        setCurrentTrack(song);

        await TrackPlayer.play();
      }
    } catch (error) {
      console.log('Error: To play the song', error);
    }
  }, []);

  const openModal = useCallback(() => {
    setModalVisible(true);
  }, []);

  const closeModal = useCallback(() => {
    setModalVisible(!modalVisible);
  }, [modalVisible]);

  const getPlayList = () => {
    const storage = new MMKV();
    const userPlayListString = storage.getString('userPlayList');

    if (userPlayListString) {
      try {
        const userPlayList = JSON.parse(userPlayListString);
        setPlayList(userPlayList);
      } catch (error) {
        console.error('Error parsing userPlayList:', error);
        setPlayList([]);
      }
    } else {
      setPlayList([]);
    }
  };

  useEffect(() => {
    if (selectedTab === tabItems[1].id) {
      getPlayList();
    }
  }, [selectedTab]);

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={require('../../assets/images/colorBg.jpg')}
          style={styles.image}
        />
        <View style={styles.textContainer}>
          <Text style={styles.cardHeading}>New Album by Coldplay</Text>
          <View>
            <Text style={styles.generalText}>Music of the</Text>
            <Text style={styles.generalText}>Spheres</Text>
          </View>
        </View>
      </View>

      <View>
        <FlatList
          data={tabItems}
          horizontal
          renderItem={({item}) => (
            <TouchableOpacity
              style={[
                selectedTab === item.id ? styles.activeTabBg : null,
                styles.tabContainer,
              ]}
              onPress={() => handleTabChange(item.id)}>
              <Text style={styles.item}>{item.title}</Text>
            </TouchableOpacity>
          )}
          keyExtractor={item => item.id}
        />
      </View>

      {selectedTab === tabItems[0].id ? (
        <View style={[{marginBottom: tabBarHeight}, styles.albumContainer]}>
          <FlatList
            data={userFavoriteSongs}
            numColumns={2}
            renderItem={({item}) => (
              <View style={styles.albumItem}>
                <Image
                  source={require('../../assets/images/151.jpg')}
                  style={styles.albumImg}
                  resizeMode="cover"
                />
                <View style={styles.controlIconWrapper}>
                  {currentTrack && currentTrack.url === item.url ? (
                    <TouchableOpacity
                      onPress={() => playSong(item)}
                      style={styles.controllerPlayer}>
                      <AntIcon
                        name="pausecircle"
                        size={24}
                        color={styles.primaryText}
                      />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      onPress={() => playSong(item)}
                      style={styles.controllerPlayer}>
                      <EntypoIcon
                        name="controller-play"
                        color={styles.primaryText}
                        size={24}
                      />
                    </TouchableOpacity>
                  )}
                </View>
                <View style={styles.albumContentContainer}>
                  <Text style={styles.albumHeadingText} numberOfLines={1}>
                    {item.name}
                  </Text>
                  <Text style={styles.albumContentText}>Unknown</Text>
                </View>
              </View>
            )}
            keyExtractor={item => item.url}
          />
        </View>
      ) : null}

      {selectedTab === tabItems[1].id ? (
        playList.length ? (
          <View style={styles.userPlayListContainer}>
            <FlatList
              data={playList}
              horizontal
              renderItem={({item}) => (
                <TouchableOpacity style={styles.userListItemWrapper}>
                  <Image
                    source={require('../../assets/images/image.png')}
                    style={styles.playListImg}
                    resizeMode="cover"
                  />
                  <View style={styles.innerWrapper}>
                    <Text style={styles.playListName}>{item.name}</Text>
                    <View style={styles.playListInfoWrapper}>
                      <Text style={styles.text}>Playlist</Text>
                      <View style={styles.dot} />
                      <Text style={styles.text}>Unknown</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
              keyExtractor={item => item.name}
            />
          </View>
        ) : (
          <View
            style={[{marginBottom: tabBarHeight}, styles.playListContainer]}>
            <View style={styles.innerContainer}>
              <Text style={styles.playListGeneralText}>
                Looking for your playlists?
              </Text>
              <Text style={styles.playListSecondaryText}>
                Playlists you create will appear here.
              </Text>

              <TouchableOpacity
                style={styles.addNewPlayListContainer}
                onPress={openModal}>
                <Text style={styles.addNewPlayListText}>New Playlist</Text>
              </TouchableOpacity>
            </View>
          </View>
        )
      ) : null}

      {modalVisible ? (
        <AddPlayList closeModal={closeModal} modalVisible={modalVisible} />
      ) : null}
    </View>
  );
};

export default UserLibrary;
