import React, {useState} from 'react';
import {Modal, Text, TextInput, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {MMKV} from 'react-native-mmkv';

import AntIcon from 'react-native-vector-icons/AntDesign';

import styles from './styles/userLibrary';

const AddPlayList = ({modalVisible, closeModal}) => {
  const [playListName, setPlayListName] = useState('');

  const onChangeText = name => {
    setPlayListName(name);
  };

  const addPlayList = () => {
    const storage = new MMKV();
    const today = new Date().toISOString().split('T')[0];

    const playlistPayload = {
      name: playListName,
      date: today,
      song: [],
    };

    const userPlayList = storage.getString('userPlayList');
    let playlists = [];
    if (userPlayList) {
      playlists = JSON.parse(userPlayList);
    }

    playlists = [...playlists, playlistPayload];
    storage.set('userPlayList', JSON.stringify(playlists));
    closeModal();
  };

  return (
    <>
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View>
              <Text style={styles.title}>Give your playlist a name</Text>
              <TextInput
                placeholder="My playlist #1"
                placeholderTextColor="#fff"
                style={styles.playlistInput}
                onChangeText={onChangeText}
                value={playListName}
              />
              <TouchableOpacity style={styles.btnWrapper} onPress={addPlayList}>
                <Text style={styles.addPlayListBtn}>Create</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={styles.closeIcon} onPress={closeModal}>
              <AntIcon name="close" size={24} color="#fff" />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

AddPlayList.propTypes = {
  modalVisible: PropTypes.bool,
  closeModal: PropTypes.func,
};

AddPlayList.default = {
  modalVisible: false,
  closeModal: () => {},
};

export default AddPlayList;
