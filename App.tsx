import React, {useCallback, useEffect} from 'react';
import 'react-native-gesture-handler';
import {PermissionsAndroid} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import {MMKV} from 'react-native-mmkv';

import BottomTabNavigation from './src/navigations';

import isPlayerInitialized from './src/utility/helper';
import PlayList from './src/screens/playlist/playList';

const Stack = createNativeStackNavigator();

const App = () => {
  const setUpTrackPlayer = () => {
    let initialSetUpTrackPlayer = false;
    const storage = new MMKV();
    storage.set('isPlayerSetup', initialSetUpTrackPlayer);

    if (!initialSetUpTrackPlayer) {
      isPlayerInitialized();
      initialSetUpTrackPlayer = true;
    }
  };

  const requestStoragePermission = useCallback(async () => {
    try {
      const storage = new MMKV();

      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: 'App Storage Permission',
          message: 'App needs access to your storage',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      const hasValidPermission = granted === PermissionsAndroid.RESULTS.GRANTED;

      storage.set('hasExternalStoragePermission', hasValidPermission);
    } catch (error) {
      console.log('Error: To get Permission', error);
    }
  }, []);

  useEffect(() => {
    requestStoragePermission();
    setUpTrackPlayer();
  }, [requestStoragePermission]);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Main"
          component={BottomTabNavigation}
          options={{headerShown: false}}
        />
        <Stack.Screen name="PlayListScreen" component={PlayList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
